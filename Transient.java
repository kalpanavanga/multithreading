package com.innominds.Thread;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Transient implements Serializable {

	int a = 10, b = 20;
	transient int c = 30;

	transient static int d = 40;
	transient final int e = 50;

	public static void main(String[] args) throws Exception {
		Transient input = new Transient();

		FileOutputStream fos = new FileOutputStream("abc.txt");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(input);

		FileInputStream fis = new FileInputStream("abc.txt");
		ObjectInputStream ois = new ObjectInputStream(fis);
		Transient output = (Transient) ois.readObject();
		System.out.println("i = " + output.a);
		System.out.println("j = " + output.b);
		System.out.println("k = " + output.c);
		System.out.println("l = " + output.d);
		System.out.println("m = " + output.e);
	}
}
