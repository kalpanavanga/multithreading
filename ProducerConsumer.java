package com.innominds.Thread;

import java.util.LinkedList;

public class ProducerConsumer {

	LinkedList<Integer> list = new LinkedList();

	public void Produce() throws InterruptedException {
		int value = 0;
		while (true) {
			synchronized (this) {

				while (list.size() > 0)
					wait();

				System.out.println("Producer data" + value);
				list.add(value);
				value++;
				notify();
				Thread.sleep(1000);
			}
		}
	}

	public void Consumer() throws InterruptedException {

		while (true) {
			synchronized (this) {
				while (list.size() == 0) {
					wait();
					int value = list.removeFirst();

					System.out.println("consumer data" + value);

					notify();
					Thread.sleep(1000);

				}
			}
		}
	}

	public static void main(String[] args) throws InterruptedException {

		ProducerConsumer pc = new ProducerConsumer();
		Thread t1 = new Thread(new Runnable() {

			public void run() {
				try {
					pc.Produce();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		Thread t2 = new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					pc.Consumer();

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		t1.start();
		t2.start();
		t1.join();
		t2.join();

	}

}
