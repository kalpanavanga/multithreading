package com.innominds.Thread;

public class EvenOrOdd {
	static int count = 1;

	public static void main(String... args) throws InterruptedException {
		int k1 = Integer.parseInt(args[0]);
		int k2 = Integer.parseInt(args[1]);
		String message1 = args[2];
		String message2 = args[3];
		int n = Integer.parseInt(args[4]);

		Thread t1 = new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {

					while (count <= n) {
						if (count % 2 == 1) {
							System.out.println(Thread.currentThread().getName() + " " + count);
							count++;
						}
						Thread.sleep(k1);
					}

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notify();

			}
		}, message1);
		Thread t2 = new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {

					while (count <= n) {
						if (count % 2 == 0) {
							System.out.println(Thread.currentThread().getName() + " " + count);
							count++;
						}
						Thread.sleep(k2);
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				notify();
			}
		}, message2);
		t1.start();
		t2.start();
	}
}