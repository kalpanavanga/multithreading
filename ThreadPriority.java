package com.innominds.Thread;

public class ThreadPriority implements Runnable {

	public final static int MIN_PRIORITY = 1;

	public final static int NOR_PRIORITY = 5;

	public final static int MAX_PRIORITY = 10;

	@Override
	public void run() {
		for (int i = 1; i <= 5; i++) {

			System.out.println(Thread.currentThread().getName() + "\t" + "i =" + i);
			try {
				Thread.sleep(1000);

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {

		ThreadPriority r = new ThreadPriority();
		Thread t1 = new Thread(r, "T1");
		Thread t2 = new Thread(r, "T2");
		Thread t3 = new Thread(r, "T3");
		Thread t4 = new Thread(r, "T4");
		Thread t5 = new Thread(r, "T5");

		t1.setPriority(Thread.MIN_PRIORITY);
		t2.setPriority(Thread.MAX_PRIORITY);
		System.out.println(t2);
		t3.setPriority(Thread.MAX_PRIORITY);
		System.out.println(t3);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
	}

}
